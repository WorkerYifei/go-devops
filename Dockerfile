# stage 1: build
FROM golang:1.17.6-buster as builder
WORKDIR /go/src/app
COPY . .
ENV GOPROXY=https://goproxy.cn,direct
ENV GO111MODULE="on"
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o target_app

# stage 2: package
# A minimal Docker image based on Alpine Linux with a complete package index and only 5 MB in size!
# @see: https://hub.docker.com/_/alpine?tab=description
FROM alpine:3.15.0
COPY --from=builder /go/src/app/target_app .
# todo 补充外部环境变量
ENTRYPOINT ["./target_app"]

# 构建命令
# docker build -f ./Dockerfile .  -t demo:v2
